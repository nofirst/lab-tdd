package com.labtest.tddyiyi;

/**
 * @Author SuJianFeng
 * @Date 2019/1/10 8:58
 * 张三的接口
 **/
public interface Calculator {
    int sum(int begin, int end);
}
