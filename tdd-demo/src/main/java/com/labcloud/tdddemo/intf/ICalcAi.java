package com.labcloud.tdddemo.intf;

/**
 * @Author 苏建锋
 * @create 2019-01-10 0:01
 */
public interface ICalcAi {
    public int divByAdd(int a, int b, int x);
}
